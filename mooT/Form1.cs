﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Collections;

namespace mooT
{
    public partial class Form1 : Form
    {
        static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
        static readonly IntPtr HWND_NOTOPMOST = new IntPtr(-2);
        static readonly IntPtr HWND_TOP = new IntPtr(0);
        static readonly IntPtr HWND_BOTTOM = new IntPtr(1);

        [Flags()]
        private enum SetWindowPosFlags : uint
        {
            /// <summary>If the calling thread and the thread that owns the window are attached to different input queues, 
            /// the system posts the request to the thread that owns the window. This prevents the calling thread from 
            /// blocking its execution while other threads process the request.</summary>
            /// <remarks>SWP_ASYNCWINDOWPOS</remarks>
            AsynchronousWindowPosition = 0x4000,
            /// <summary>Prevents generation of the WM_SYNCPAINT message.</summary>
            /// <remarks>SWP_DEFERERASE</remarks>
            DeferErase = 0x2000,
            /// <summary>Draws a frame (defined in the window's class description) around the window.</summary>
            /// <remarks>SWP_DRAWFRAME</remarks>
            DrawFrame = 0x0020,
            /// <summary>Applies new frame styles set using the SetWindowLong function. Sends a WM_NCCALCSIZE message to 
            /// the window, even if the window's size is not being changed. If this flag is not specified, WM_NCCALCSIZE 
            /// is sent only when the window's size is being changed.</summary>
            /// <remarks>SWP_FRAMECHANGED</remarks>
            FrameChanged = 0x0020,
            /// <summary>Hides the window.</summary>
            /// <remarks>SWP_HIDEWINDOW</remarks>
            HideWindow = 0x0080,
            /// <summary>Does not activate the window. If this flag is not set, the window is activated and moved to the 
            /// top of either the topmost or non-topmost group (depending on the setting of the hWndInsertAfter 
            /// parameter).</summary>
            /// <remarks>SWP_NOACTIVATE</remarks>
            DoNotActivate = 0x0010,
            /// <summary>Discards the entire contents of the client area. If this flag is not specified, the valid 
            /// contents of the client area are saved and copied back into the client area after the window is sized or 
            /// repositioned.</summary>
            /// <remarks>SWP_NOCOPYBITS</remarks>
            DoNotCopyBits = 0x0100,
            /// <summary>Retains the current position (ignores X and Y parameters).</summary>
            /// <remarks>SWP_NOMOVE</remarks>
            IgnoreMove = 0x0002,
            /// <summary>Does not change the owner window's position in the Z order.</summary>
            /// <remarks>SWP_NOOWNERZORDER</remarks>
            DoNotChangeOwnerZOrder = 0x0200,
            /// <summary>Does not redraw changes. If this flag is set, no repainting of any kind occurs. This applies to 
            /// the client area, the nonclient area (including the title bar and scroll bars), and any part of the parent 
            /// window uncovered as a result of the window being moved. When this flag is set, the application must 
            /// explicitly invalidate or redraw any parts of the window and parent window that need redrawing.</summary>
            /// <remarks>SWP_NOREDRAW</remarks>
            DoNotRedraw = 0x0008,
            /// <summary>Same as the SWP_NOOWNERZORDER flag.</summary>
            /// <remarks>SWP_NOREPOSITION</remarks>
            DoNotReposition = 0x0200,
            /// <summary>Prevents the window from receiving the WM_WINDOWPOSCHANGING message.</summary>
            /// <remarks>SWP_NOSENDCHANGING</remarks>
            DoNotSendChangingEvent = 0x0400,
            /// <summary>Retains the current size (ignores the cx and cy parameters).</summary>
            /// <remarks>SWP_NOSIZE</remarks>
            IgnoreResize = 0x0001,
            /// <summary>Retains the current Z order (ignores the hWndInsertAfter parameter).</summary>
            /// <remarks>SWP_NOZORDER</remarks>
            IgnoreZOrder = 0x0004,
            /// <summary>Displays the window.</summary>
            /// <remarks>SWP_SHOWWINDOW</remarks>
            ShowWindow = 0x0040,
        }

        private Dictionary<string, IntPtr> m_procHandles = new Dictionary<string, IntPtr>();
        private List<IntPtr> m_topWindows = new List<IntPtr>();

        private bool m_bTooltipDisplayed = false;
        private bool m_bClosing = false;

        public Form1()
        {
            InitializeComponent();
            RefreshWindows();
        }

        private void RefreshWindows()
        {
            Process[] procs = Process.GetProcesses();
            IntPtr hWnd;

            // Clear collection
            m_procHandles.Clear();

            // Reset context menu
            contextMenuStrip1.SuspendLayout();
            for (int i = (contextMenuStrip1.Items.Count - 1); i > 3; i--)
            {
                if(contextMenuStrip1.Items[i] != null)
                    contextMenuStrip1.Items.RemoveAt(i);
            }

            // Add separator
            if (procs.Length > 0)
                contextMenuStrip1.Items.Add(new ToolStripSeparator());

            foreach (Process proc in procs)
            {
                if ((hWnd = proc.MainWindowHandle) != IntPtr.Zero)
                {
                    if (m_procHandles.ContainsKey(proc.ProcessName))
                        continue;

                    m_procHandles.Add(proc.ProcessName, proc.MainWindowHandle);

                    // Populate context menu strip
                    ToolStripMenuItem temp = new ToolStripMenuItem(proc.ProcessName){CheckOnClick = true};
                    if (m_topWindows.Contains(proc.MainWindowHandle))
                        temp.Checked = true;

                    contextMenuStrip1.Items.Add(temp);
                }
            }

            listBox1.DataSource = new BindingSource(m_procHandles, null);
            listBox1.DisplayMember = "Key";
            listBox1.ValueMember = "Value";

            contextMenuStrip1.ResumeLayout();
        }

        [DllImport("user32.dll", SetLastError = true)]
        static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, SetWindowPosFlags uFlags);
        [DllImport("user32.dll")]
        private static extern bool SetForegroundWindow(IntPtr hWnd);
        [DllImport("user32.dll")]
        private static extern bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);
        [DllImport("user32.dll")]
        private static extern bool IsIconic(IntPtr hWnd);
        private void ToggleWindow(IntPtr hWnd)
        {
            if (!m_topWindows.Contains(hWnd))
            {
                // Show the desired window
                if(IsIconic(hWnd))
                    ShowWindowAsync(hWnd, 9);

                // Bring window to foreground
                SetForegroundWindow(hWnd);

                SetWindowPos((IntPtr)hWnd, HWND_TOPMOST, 0, 0, 0, 0, SetWindowPosFlags.IgnoreMove | SetWindowPosFlags.IgnoreResize | SetWindowPosFlags.ShowWindow);
                m_topWindows.Add(hWnd);
            }
            else
            {
                SetWindowPos((IntPtr)hWnd, HWND_NOTOPMOST, 0, 0, 0, 0, SetWindowPosFlags.IgnoreMove | SetWindowPosFlags.IgnoreResize | SetWindowPosFlags.ShowWindow);
                m_topWindows.Remove(hWnd);
            }
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (!m_bClosing)
            {
                Hide();

                if (!m_bTooltipDisplayed)
                {
                    notifyIcon1.ShowBalloonTip(1000, string.Empty, "mooT is still running", ToolTipIcon.None);
                    m_bTooltipDisplayed = true;
                }

                e.Cancel = true;
            }
            else
                base.OnFormClosing(e);
        }

        #region ** Event Handlers **

        private void btnToggle_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
                ToggleWindow((IntPtr)listBox1.SelectedValue);
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
            {
                Hide();

                if (!m_bTooltipDisplayed)
                {
                    notifyIcon1.ShowBalloonTip(1000, string.Empty, "mooT is still running", ToolTipIcon.None);
                    m_bTooltipDisplayed = true;
                }
            }
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            RefreshWindows();
            Show();
            WindowState = FormWindowState.Normal;
        }

        private void tsRestore_Click(object sender, EventArgs e)
        {
            RefreshWindows();
            Show();
            WindowState = FormWindowState.Normal;
            contextMenuStrip1.Close();
        }

        private void tsAllBottom_Click(object sender, EventArgs e)
        {
            foreach (IntPtr hWnd in m_topWindows)
            {
                SetWindowPos((IntPtr)hWnd, HWND_NOTOPMOST, 0, 0, 0, 0, SetWindowPosFlags.IgnoreMove | SetWindowPosFlags.IgnoreResize | SetWindowPosFlags.ShowWindow);
            }

            m_topWindows.Clear();
            contextMenuStrip1.Close();
        }

        private void tsClose_Click(object sender, EventArgs e)
        {
            m_bClosing = true;
            contextMenuStrip1.Close();
            Application.Exit();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshWindows();
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            RefreshWindows();
        }

        private void contextMenuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (m_procHandles.ContainsKey(e.ClickedItem.Text))
            {
                ToggleWindow((IntPtr)m_procHandles[e.ClickedItem.Text]);
            }
        }

        private void contextMenuStrip1_MouseLeave(object sender, EventArgs e)
        {
            contextMenuStrip1.Close();
        }

        #endregion
    }
}
